using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace Digger
{
    #region DiggerPRO

    /// <summary>
    /// To support real-time / in-game editing, add this component into the scene and use its 'Modify' method to edit the terrain.
    /// </summary>
    /// <see cref="DiggerRuntimeUsageExample">Example of use</see>
    public class DiggerMasterRuntime : MonoBehaviour
    {
        public bool enablePersistence;

        private DiggerSystem[] diggerSystems;

        /// <summary>
        /// Modify the terrain at runtime by performing the requested action.
        /// </summary>
        /// <param name="position">Position where you want to edit the terrain</param>
        /// <param name="brush">Brush type</param>
        /// <param name="action">Action type</param>
        /// <param name="textureIndex">Index of the texture to be used (starting from 0 to 7). See DiggerMaster inspector to know what texture corresponds to an index.</param>
        /// <param name="opacity">Strength/intensity of edit</param>
        /// <param name="size">Size of edit</param>
        /// <param name="removeDetails">Remove terrain details in range</param>
        /// <param name="removeTreesInSphere">Remove terrain trees in range</param>
        /// <param name="stalagmiteHeight">Height of stalagmite (only when Brush is stalagmite)</param>
        /// <param name="stalagmiteUpsideDown">Defines if stalagmite is upside-down or not (only when Brush is stalagmite)</param>
        public void Modify(Vector3 position, BrushType brush, ActionType action, int textureIndex, float opacity,
                           float size, bool removeDetails = true, bool removeTreesInSphere = true, float stalagmiteHeight = 8f, bool stalagmiteUpsideDown = false)
        {
            if (!CheckEditor())
                return;

            if (action == ActionType.Smooth) {
                Debug.LogError("Smooth action is not supported at runtime");
                return;
            }

            foreach (var diggerSystem in diggerSystems) {
                diggerSystem.Modify(brush, action, opacity, position, size, stalagmiteHeight, stalagmiteUpsideDown, textureIndex, removeDetails);
                if (removeTreesInSphere) {
                    diggerSystem.RemoveTreesInSphere(position, size);
                }
            }
        }

        public void PersistAll()
        {
#if !UNITY_EDITOR
            foreach (var diggerSystem in diggerSystems) {
                diggerSystem.PersistAtRuntime();
            }
#endif
        }

        private void Awake()
        {
            diggerSystems = FindObjectsOfType<DiggerSystem>();
            foreach (var diggerSystem in diggerSystems) {
                Init(diggerSystem);
            }
        }

        private void Init(DiggerSystem diggerSystem)
        {
            if (diggerSystem.IsInitialized)
                return;

            diggerSystem.PreInit(Application.isEditor || enablePersistence);
            diggerSystem.Init(true);
        }

        private bool CheckEditor()
        {
#if UNITY_EDITOR
            if (EditorPrefs.GetBool(DiggerMaster.PersistModificationsInPlayModeEditorKey, true)) {
                EditorUtility.DisplayDialog("Can't use DiggerMasterRuntime when 'Persist in Play Mode' is enabled",
                                            "You have to disable 'Persist in Play Mode' from the DiggerMaster inspector to be able to use " +
                                            "DiggerMasterRuntime script.", "Ok");
                return false;
            }

            return true;
#else
            return true;
#endif
        }
    }

    #endregion
}