namespace Digger
{
    public enum TerrainMaterialType
    {
        Standard,
        LWRP,
        CTS
    }
}