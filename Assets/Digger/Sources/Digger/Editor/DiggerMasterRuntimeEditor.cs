using UnityEditor;

namespace Digger
{
    #region DiggerPRO

    [CustomEditor(typeof(DiggerMasterRuntime))]
    public class DiggerMasterRuntimeEditor : Editor
    {
        private DiggerMasterRuntime diggerMasterRuntime;

        public void OnEnable()
        {
            diggerMasterRuntime = (DiggerMasterRuntime) target;
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.HelpBox("This script allows Digger to work at runtime, for real-time / in-game digging.\n\n" +
                                    "It has a public method named 'Modify' that you must call from your scripts to edit the terrain.\n\n" +
                                    "See 'DiggerRuntimeUsageExample.cs' in Assets/Digger/Demo for a working example.", MessageType.Info);

            EditorGUILayout.LabelField("Runtime Options", EditorStyles.boldLabel);
            diggerMasterRuntime.enablePersistence = EditorGUILayout.ToggleLeft("Enable persistence features at runtime", diggerMasterRuntime.enablePersistence);
            if (diggerMasterRuntime.enablePersistence) {
                EditorGUILayout.HelpBox("As persistence features at runtime are enabled, you will be able to use " +
                                        "DiggerMasterRuntime.Persist method.\n" +
                                        "Note: this may have a little impact on performance.", MessageType.Warning);
            } else {
                EditorGUILayout.HelpBox("As persistence features at runtime are disabled, you won't be able to use " +
                                        "DiggerMasterRuntime.Persist method.", MessageType.Warning);
            }
        }
    }

    #endregion
}