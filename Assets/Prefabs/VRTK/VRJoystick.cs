﻿using System;
using System.Collections;
using UnityEngine;
using VRTK;

public class VRJoystick : VRTK_InteractableObject
{
    public Action OnUseStarted;
    public Action OnUseStopped;

    [Header("Joystick")]
    [SerializeField]
    Transform _handle;

    [SerializeField]
    float _maxXAngle = 45;

    [SerializeField]
    float _maxZAngle = 45;

    [SerializeField]
    [Range(0, 1)]
    float _vibrationStrenght = .1f;

    [SerializeField]
    [Range(0, 1)]
    float _vibrationInterval = .05f;

    public float XPercentage;
    public float ZPercentage;
    public bool invertXAxis;
    public bool invertZAxis;

    protected VRTK_ControllerEvents _controllerEvents;

    bool _grabbing;

    override protected void Update()
    {
        base.Update();

        var angleX = _handle.localRotation.eulerAngles.x;
        if (angleX > 180)
            angleX -= 360;

        var angleZ = _handle.localRotation.eulerAngles.z;
        if (angleZ > 180)
            angleZ -= 360;

        XPercentage = angleX / _maxXAngle;
        ZPercentage = angleZ / _maxZAngle;
        if (invertXAxis) XPercentage = -XPercentage;
        if (invertZAxis) ZPercentage = -ZPercentage;
    }

    public override void StartTouching(VRTK_InteractTouch currentTouchingObject)
    {
        base.StartTouching(currentTouchingObject);
        _controllerEvents = currentTouchingObject.GetComponent<VRTK_ControllerEvents>();
        Vibrate(.5f);
    }

    public override void StopTouching(VRTK_InteractTouch previousTouchingObject)
    {
        base.StopTouching(previousTouchingObject);
        _controllerEvents = previousTouchingObject.GetComponent<VRTK_ControllerEvents>();
        Vibrate(.5f);
    }

    override public void Grabbed(VRTK_InteractGrab grabbingObject)
    {
        base.Grabbed(grabbingObject);
        _controllerEvents = grabbingObject.GetComponent<VRTK_ControllerEvents>();
        Vibrate(.5f);
        _grabbing = true;

        StartCoroutine(DetectJoystickMovement());
    }

    override public void Ungrabbed(VRTK_InteractGrab previousGrabbingObject)
    {
        base.Ungrabbed(previousGrabbingObject);

        _grabbing = false;
    }

    IEnumerator DetectJoystickMovement()
    {
        var currentXPercentage = XPercentage;
        var currentZPercentage = ZPercentage;
        while (_grabbing)
        {
            if (Mathf.Abs(currentXPercentage - XPercentage) > _vibrationInterval)
            {
                Vibrate(_vibrationStrenght);
                currentXPercentage = XPercentage;
            }
            else if (Mathf.Abs(currentZPercentage - ZPercentage) > _vibrationInterval)
            {
                Vibrate(_vibrationStrenght);
                currentZPercentage = ZPercentage;
            }

            yield return null;
        }
    }

    public override void StartUsing(VRTK_InteractUse currentUsingObject)
    {
        base.StartUsing(currentUsingObject);

        if (OnUseStarted != null)
            OnUseStarted();
    }

    public override void StopUsing(VRTK_InteractUse previousUsingObject, bool resetUsingObjectState = true)
    {
        base.StopUsing(previousUsingObject);

        if (OnUseStopped != null)
            OnUseStopped();
    }

    public void Vibrate(float vibrationStrength)
    {
        if (_controllerEvents)
            VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(_controllerEvents.gameObject), vibrationStrength, 0.1f, 0.01f);
    }
}