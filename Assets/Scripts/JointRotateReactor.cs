﻿using UnityEngine;
using UnityEngine.UI;
using VRTK.Controllables;

// Для стандартных одноосевых рычагов
public class JointRotateReactor : MonoBehaviour
{
    public VRTK_BaseControllable controllable;
    public Text displayText;

    public Transform joint;      // стрела или ковш экскаватора
    private float rotValue;      // данные с рычага
    public float speed = 1f;
    public float beginRotationThreshold = 1f;
    public Vector3 rotateAroundAxis = Vector3.up;

    protected virtual void OnEnable()
    {
        controllable = (controllable == null ? GetComponent<VRTK_BaseControllable>() : controllable);
        controllable.ValueChanged += ValueChanged;
        rotateAroundAxis.Normalize();
    }

    protected virtual void ValueChanged(object sender, ControllableEventArgs e)
    {
        if (displayText != null)
        {
            displayText.text = e.value.ToString("F1");
        }

        rotValue = e.value;
    }

    private void Update()
    {
        if (rotValue < -beginRotationThreshold || rotValue > beginRotationThreshold)
        {
            joint.Rotate(rotateAroundAxis, rotValue * speed * Time.deltaTime);
        }
    }
}

