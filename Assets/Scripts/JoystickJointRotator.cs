﻿using UnityEngine;

// Вращение стрелы джойстиком
public class JoystickJointRotator : MonoBehaviour
{
    public VRJoystick joystick;
    public Transform jointX;
    public Transform jointZ;
    public float speedX = 10f;
    public float speedZ = 10f;
    public float beginRotationThreshold = .1f;
    public Vector3 rotateAroundXAxis = Vector3.up;
    public Vector3 rotateAroundZAxis = Vector3.forward;
    [HideInInspector]
    public bool isXRotating, isZRotating;       // нужно для JointRotationSound.cs

    private void Update()
    {
        if (joystick.XPercentage < -beginRotationThreshold || joystick.XPercentage > beginRotationThreshold)
        {
            jointX.Rotate(rotateAroundXAxis, joystick.XPercentage * speedX * Time.deltaTime);            
            isXRotating = true;
        }
        else isXRotating = false;

        if (joystick.ZPercentage < -beginRotationThreshold || joystick.ZPercentage > beginRotationThreshold)
        {
            jointZ.Rotate(rotateAroundZAxis, joystick.ZPercentage * speedZ * Time.deltaTime);
            isZRotating = true;
        }
        else isZRotating = false;
    }
}
