﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;

public class SandBallsSpawner : MonoBehaviour
{
    public GameObject sandball;             // префаб шарика    
    public float radius;                    // радиус сферы спавна
    public int count;                       // количество шариков за один раз

    // вызов из ивента коллизии ковша с землей
    public void Spawn()
    {
        for (int i = 0; i < count; i++)
        {
            Vector3 rndPos = transform.position + Random.insideUnitSphere * radius;            
            LeanPool.Spawn(sandball, rndPos, Quaternion.identity);
        }
    }
}
