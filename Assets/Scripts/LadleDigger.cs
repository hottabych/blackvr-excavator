﻿using Digger;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LadleDigger : MonoBehaviour
{
    DiggerMasterRuntime diggerMasterRuntime;
    public Transform raycaster;
    public float raycastLength = 1f;
    public ParticleSystem groundParticles;
    public ParticleSystem dustParticles;
    public UnityEvent OnDiggerHit;

    private void Awake()
    {
        diggerMasterRuntime = FindObjectOfType<DiggerMasterRuntime>();
    }

    private void Update()
    {
        if (DiggerPhysics.Raycast(raycaster.position, Vector3.down, out var hit, 1f))
        {
            diggerMasterRuntime.Modify(hit.point, BrushType.RoundedCube, ActionType.Dig, 1, .5f, raycastLength);
            //groundParticles.Play();
            dustParticles.Play();
            OnDiggerHit.Invoke();
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Vector3 direction = raycaster.TransformDirection(Vector3.forward) * raycastLength;
        Gizmos.DrawRay(raycaster.position, direction);
    }
}
