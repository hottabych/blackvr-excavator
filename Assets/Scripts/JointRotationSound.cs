﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JointRotationSound : MonoBehaviour
{
    JoystickJointRotator jointRotator;
    private new AudioSource audio;


    private void Awake()
    {
        jointRotator = GetComponent<JoystickJointRotator>();
        audio = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (jointRotator.isXRotating || jointRotator.isZRotating)
        {
            if (!audio.isPlaying) audio.Play();
        }
        else
        {
            if (audio.isPlaying) audio.Stop();
        }
    }
}
